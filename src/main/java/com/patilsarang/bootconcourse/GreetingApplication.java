package com.patilsarang.bootconcourse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * This is main application start point for greeting microservice
 */
@SpringBootApplication
public class GreetingApplication {

	public static void main(String[] args) {
		System.out.println("general spring start up");
		SpringApplication.run(GreetingApplication.class, args);
	}

}
