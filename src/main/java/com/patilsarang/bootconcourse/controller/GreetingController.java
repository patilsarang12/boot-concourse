/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.patilsarang.bootconcourse.controller;

import java.util.concurrent.atomic.AtomicLong;

import com.patilsarang.bootconcourse.model.Greeting;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Simple implementation of {@link ClientHttpResponse} that reads the response's body into
 * memory, thus allowing for multiple invocations of.
 *
 * @author Arjen Poutsma
 * @since 3.1
 */
@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";

	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/greeting")
	public @ResponseBody Greeting greeting(
			@RequestParam(value = "name", required = false, defaultValue = "world") String name) {
		return new Greeting(this.counter.incrementAndGet(), String.format(template, name));
	}

}
