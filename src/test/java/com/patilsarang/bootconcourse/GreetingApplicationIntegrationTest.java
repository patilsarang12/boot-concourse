package com.patilsarang.bootconcourse;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(classes = GreetingApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class GreetingApplicationIntegrationTest {

	@Test
	public void contextLoads() {

	}

}
