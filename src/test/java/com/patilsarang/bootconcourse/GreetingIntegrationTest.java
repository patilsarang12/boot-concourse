package com.patilsarang.bootconcourse;

import com.patilsarang.bootconcourse.controller.GreetingController;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(GreetingController.class)
public class GreetingIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void returnsACorrectResponse() throws Exception {

		this.mockMvc.perform(get("/greeting")).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.content", is(containsString("Hello, world!"))));

	}

	/**
	 * adding documentation stuff
	 */
	@Test
	public void returnsAPersonalisedMessage() throws Exception {
		this.mockMvc.perform(get("/greeting?name=Emanuele").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("content", is("Hello, Emanuele!")));

	}

}
